<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function oa_group_links_user_default_permissions() {
  $permissions = array();

  // Exported permission: create group_links content
  $permissions['create group_links content'] = array(
    'name' => 'create group_links content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: delete any group_links content
  $permissions['delete any group_links content'] = array(
    'name' => 'delete any group_links content',
    'roles' => array(),
  );

  // Exported permission: delete own group_links content
  $permissions['delete own group_links content'] = array(
    'name' => 'delete own group_links content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: edit any group_links content
  $permissions['edit any group_links content'] = array(
    'name' => 'edit any group_links content',
    'roles' => array(),
  );

  // Exported permission: edit own group_links content
  $permissions['edit own group_links content'] = array(
    'name' => 'edit own group_links content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  return $permissions;
}
