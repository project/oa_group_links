<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function oa_group_links_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'oa_group_links';
  $context->description = '';
  $context->tag = 'Group Links';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'group_links' => 'group_links',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'group_links' => 'group_links',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
    'menu' => 'features',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Group Links');
  $export['oa_group_links'] = $context;

  return $export;
}
