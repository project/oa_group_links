<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function oa_group_links_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function oa_group_links_node_info() {
  $items = array(
    'group_links' => array(
      'name' => t('Links'),
      'module' => 'features',
      'description' => t('Custom Links for groups.'),
      'has_title' => '1',
      'title_label' => t('Link Name'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function oa_group_links_views_api() {
  return array(
    'api' => '2',
  );
}
